# coding=utf-8

"""Genomic variation synthesiser workflow.

This Snakefile requires a configuration file having at least these entries:

    ---
    datasets:
      - "dataset1"
      - "dataset2"
      - ...

    # Either specify input genome sequence for each dataset:
    genome:
      dataset1: "/path/to/its_genome.fa"
      dataset2: "/path/to/another_genome.fasta"
      ...
    # Or use dataset wildcard (pay attention to trailing 's' in genome*s*).
    dataset_genomes: "/path/to/{dataset}/genome.fa[sta]"

    targets:
      - "/path/to/TARGET_A"
      - "/another/path/to/TARGET_B"
      - ...
    ...

where `TARGET_A` or `TARGET_B` can be EXACTLY one of the final targets:

- "report.html"             for generating the experiment report.
- "{dataset}/synth.done"    for generating all simulated VCF files.

The `{dataset}` pattern will be expanded for all datasets specified in
configuration file or can be manually stated; e.g "/path/to/ecoli/wg.xg".

There is also a sample configuration file in the root directory.

For more information about the workflow see README file.
"""

__author__ = "Ali Ghaffaari"
__email__ = "ali.ghaffaari@mpi-inf.mpg.de"
__organization__ = "Max-Planck Institut fuer Informatik"
__license__ = "MIT"
__version__ = "v0.0.1"


# Using `bash` for shell commands.
shell.executable("/bin/bash")
# Check for Snakemake minimum version.
snakemake.utils.min_version("3.9.0")

configfile: "config.yml"

# FIXME: revert the change
WORKFLOW_PREFIX = "https://bitbucket.org/cartoonist/synth-workflow/raw/" + __version__
WRAPPERS_PREFIX = "https://bitbucket.org/cartoonist/snakemake-wrappers/raw/develop"
BLOB_TOKEN = 'src'  # GitHub: 'blob', BitBucket: 'src', Local: ''

# Fetch targets
TARGETS = expand(config["targets"], dataset=config["datasets"])

##  Rules
rule all:
    input:
        TARGETS

include:
    WORKFLOW_PREFIX + "/rules/synth.rules"

include:
    WORKFLOW_PREFIX + "/rules/report.rules"
